import os

from tflearn import local_response_normalization

from pathos.constants import *

import tflearn
import tflearn.layers as tflayers
from pathos import loader

# The fer-2013 dataset has only 48x48 sized images
SAVE_DIRECTORY = "./saved_data/"


class EmotionRecognitionModel:

    def __init__(self, name):
        self._name = name
        self.target_classes = EMOTIONS
        self.target_classes_macros = ['ANG', 'DIS', 'FEA', 'HAP', 'SAD', 'SUR', 'NEU']
        self.datasetLoader = loader.DatasetLoader()

    def build_network(self):
        print("[INFO] Building neural network")

        # Start off with grayscale 48x48 input data
        self.network = tflayers.core.input_data(shape=[None, INPUT_SIZE, INPUT_SIZE, 1])

        # Convolution
        self.network = tflayers.conv.conv_2d(self.network, 64, 5, activation='relu')
       # self.network = local_response_normalization(self.network)
        self.network = tflayers.conv.max_pool_2d(self.network, 3, strides=2)
        self.network = tflayers.conv.conv_2d(self.network, 64, 5, activation='relu')
        self.network = tflayers.conv.max_pool_2d(self.network, 3, strides=2)
        self.network = tflayers.conv.conv_2d(self.network, 128, 4, activation='relu')

        # The fully connected layers
        self.network = tflayers.core.dropout(self.network, 0.3)
        self.network = tflayers.core.fully_connected(
            self.network,
            3072,
            activation='relu'
        )
        self.network = tflayers.core.fully_connected(
            self.network,
            len(self.target_classes),
            activation='softmax'
        )
        self.network = tflayers.estimator.regression(
            self.network,
            optimizer='momentum',
            loss='categorical_crossentropy'
        )

        self.model = tflearn.DNN(
            self.network,
            checkpoint_path=os.path.join(SAVE_DIRECTORY, self._name),
        )

        self.load_model()
        print("[INFO] Finished building neural network")

    def load_saved_dataset(self):
        self.datasetLoader.load_from_save()
        print("[INFO] Dataset found and loaded.")

    def save_model(self):
        self.model.save(os.path.join(SAVE_DIRECTORY, self._name))
        print("[INFO] Model saved.")

    def load_model(self):
        print("looking for network in " + os.path.join(SAVE_DIRECTORY, self._name + ".meta"))
        if os.path.isfile(os.path.join(SAVE_DIRECTORY, self._name + ".meta")):
            self.model.load(os.path.join(SAVE_DIRECTORY, self._name))
            print("[INFO] Model loaded from filesystem.")

    def predict(self, image):
        if image is None:
            return None
        image = image.reshape([-1, 48, 48, 1])
        return self.model.predict(image)

    def train(self):
        self.load_saved_dataset()
        self.build_network()
        if self.datasetLoader is None:
            self.load_saved_dataset()
        self.load_model()
        # Training
        print('[+] Training network')
        self.model.fit(
            self.datasetLoader.images, self.datasetLoader.labels,
            validation_set=(self.datasetLoader.images_test,
                            self.datasetLoader.labels_test),
            n_epoch=100,
            batch_size=50,
            shuffle=True,
            show_metric=True,
            snapshot_step=200,
            snapshot_epoch=True,
            run_id='emotion_recognition'
        )
