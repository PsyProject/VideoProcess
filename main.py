import os
import sys

from pathos import model

print("<< Pathos >>")


network = model.EmotionRecognitionModel("learn80")


network.train()
network.save_model()
