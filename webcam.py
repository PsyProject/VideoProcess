import cv2
import numpy as np

from imutils import face_utils
import imutils
import dlib
from pathos.constants import FACIAL_LANDMARKS_IDXS, EMOTIONS, COLORS
from pathos.model import EmotionRecognitionModel

# import matplotlib.pyplot as plt

from anatomy.eye import Eye

print("test")

#
# Some utilitary functions
#

def draw_bounding_box(face_coordinates, image_array, color):
    x, y, w, h = face_coordinates
    cv2.rectangle(image_array, (x, y), (x + w, y + h), color, 2)


def apply_offsets(face_coordinates, offsets):
    x, y, width, height = face_coordinates
    x_off, y_off = offsets
    return x - x_off, x + width + x_off, y - y_off, y + height + y_off


def draw_text(coordinates, image_array, text: str, color, x_offset=0, y_offset=0,
              font_scale=2, thickness=2):
    x, y = coordinates[:2]
    cv2.putText(image_array, text, (x + x_offset, y + y_offset),
                cv2.FONT_HERSHEY_SIMPLEX,
                font_scale, color, thickness, cv2.LINE_AA)


def format_image(bgr_image):
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)

    gray_image = cv2.equalizeHist(gray_image)
    try:
        gray_image = cv2.resize(gray_image, (48, 48), interpolation=cv2.INTER_CUBIC) / 255.
    except Exception:
        print("[Error] While resizing")
        return None

    return gray_image


print("Starting main loop")
emotions_history = list()
for i in range(len(EMOTIONS)-1):
    emotions_history.append(np.zeros(100))


# parameters for loading data and images
# detection_model_path = 'pathos/haarcascade_files/haarcascade_frontalface_default.xml'
# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor 

print("loading facial landmarks detector")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# hyper-parameters for bounding boxes shape
# frame_window = 10
# emotion_offsets = (20, 40)

# loading models
# face_detection = cv2.CascadeClassifier(detection_model_path)
# emotion_classifier = load_model(emotion_model_path, compile=False)

# emotion_network = EmotionRecognitionModel(os.path.join(SAVE_DIRECTORY, "network-1200"))
emotion_network = EmotionRecognitionModel("learn80")

emotion_network.build_network()

video_capture = cv2.VideoCapture(0)
plt.figure()
lines = [None] * (len(EMOTIONS)+1)
for i in range(len(emotions_history)):
    lines[i], = plt.plot(emotions_history[i], color=np.asarray(COLORS[i])/255, label=EMOTIONS[i])
plt.legend()
plt.axis([0, 100, 0, 1])
timestep = 0

while video_capture.isOpened():
    # Capturing the image from webcam
    bgr_image = video_capture.read()[1]

    # Converting image to grayscale and rgb versions
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    # faces = face_detection.detectMultiScale(gray_image, 1.3, 5)
    # Loop on each face
    #for face_coordinates in faces:
        #x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        # gray_face = gray_image[y1:y2, x1:x2]
        #focused_face_bgr = bgr_image[y1:y2, x1:x2]

        #result = emotion_network.predict(format_image(focused_face_bgr))

        ##if result is not None:
        #    emotion_probability = np.max(result)
        #    emotion_label_arg = np.argmax(result)
        #    emotion_text = EMOTIONS[emotion_label_arg]
        #
        #    color = emotion_probability * np.asarray(COLORS[emotion_label_arg])
        #
        #    for (proba, emotion, index) in zip(result[0], EMOTIONS, range(len(EMOTIONS))):
        #        cv2.rectangle(rgb_image, (face_coordinates[0] + face_coordinates[2], face_coordinates[1] + index * 30),
        #                      (face_coordinates[0] + face_coordinates[2] + int(proba * 100),
        #                       face_coordinates[1] + (index + 1) * 30), (200, 200, 200, 200), -1)
        #        draw_text(face_coordinates, rgb_image, emotion + "->" + str(int(proba * 100)), color,
        #                  face_coordinates[2] + 10, index * 30 + 18, 0.5, 1)
        #
        #    # emotion_window.append(emotion_text)
        #
        #    # if len(emotion_window) > frame_window:
        #    #    emotion_window.pop(0)
        #    # try:
        #    #    emotion_mode = mode(emotion_window)
        #    # except:
        #    #    continue
        #
        #    # print(emotion_text)
        #    color = color.astype(int)
        #    color = color.tolist()
        #
        #    draw_bounding_box(face_coordinates, rgb_image, color)
        ## draw_text(face_coordinates, rgb_image, emotion_mode,
        ##          color, 0, -45, 1, 1)

    # bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
    # ============================================
    # LANDMARKS
    # ============================================
    # image_landmarks = imutils.resize(bgr_image, width=500)
    image_landmarks = bgr_image
    # bgr_image = imutils.resize(bgr_image, width=500)
    gray = cv2.cvtColor(image_landmarks, cv2.COLOR_BGR2GRAY)
    

    # detect faces in the grayscale image
    rects = detector(gray, 1)
    clone = image_landmarks.copy()
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the landmark (x, y)-coordinates to a NumPy array
        # print(rect)
        x1, x2, y1, y2 = rect.tl_corner().x, rect.br_corner().x, rect.tl_corner().y, rect.br_corner().y
        face_coordinates = x1, y1, x2-x1, y2-y1
        focused_face_gray = gray[y1:y2, x1:x2]


        focused_face_gray = cv2.equalizeHist(focused_face_gray)
        focused_face_gray = cv2.resize(focused_face_gray, (48, 48), interpolation=cv2.INTER_CUBIC) / 255.
        cv2.imshow("Sampled face", focused_face_gray)
        result = emotion_network.predict(focused_face_gray)


        if result is not None:
            emotion_probability = np.max(result)
            emotion_label_arg = np.argmax(result)

            color = emotion_probability * np.asarray(COLORS[emotion_label_arg])

            for (proba, emotion, index) in zip(result[0], EMOTIONS, range(len(EMOTIONS))):
                cv2.rectangle(clone, (face_coordinates[0] + face_coordinates[2], face_coordinates[1] + index * 30),
                                (face_coordinates[0] + face_coordinates[2] + int(proba * 100),
                                face_coordinates[1] + (index + 1) * 30), (200, 200, 200, 200), -1)
                draw_text(face_coordinates, clone, emotion + "->" + str(int(proba * 100)), color,
                            face_coordinates[2] + 10, index * 30 + 18, 0.5, 1)
                if index < len(EMOTIONS)-1:
                    emotions_history[index][timestep%100] = proba
        
        for i in range(len(EMOTIONS)-1):
            lines[i].set_ydata(np.array(emotions_history[i]))
            #lines[i].set_xdata(np.array(range(len(emotions_history[i]))))
        plt.draw()
        
        
        
        
        
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        
        # loop over the face parts individually
        # draw_bounding_box(face_coordinates, clone, (0, 255, 0))
        draw_bounding_box((rect.left(), rect.top(), rect.width(), rect.height()), clone, (0, 0, 0))
        for (name, (i, j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
            # clone the original image so we can draw on it, then
            # display the name of the face part on the image
            # cv2.putText(clone, name, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
            #    0.7, (0, 0, 255), 2)

            # loop over the subset of facial landmarks, drawing the
            # specific face part
            for (x, y) in shape[i:j]:
                cv2.circle(clone, (x, y), 1, (0, 0, 255), -1)

            # Localisation du barycentre de la zone occulaire
            eye_pos_x = 0
            eye_pos_y = 0
            if name == "left_eye" or name == "right_eye":
                for (x, y) in shape[i:j]:
                    eye_pos_x += x
                    eye_pos_y += y
                eye_pos_x = eye_pos_x / (j-i)
                eye_pos_y = eye_pos_y / (j-i)

            # if name == "right_eye":
                right_eye = Eye(shape[i:j])
                print(right_eye.aspect_ratio())
                if right_eye.aspect_ratio() < 0.3:
                    print("blink !")
                else:
                    cv2.circle(clone, (int(eye_pos_x), int(eye_pos_y)), 3, (255, 0, 0))


            

            #if name == "jaw":
            #    for l in range(i + 2, j):
            #        ptA = tuple(shape[l - 1])
            #        ptB = tuple(shape[l])
            #        cv2.line(clone, ptA, ptB, (255, 0, 0), 1)
    
    cv2.imshow('Webcam', clone)
    plt.pause(0.0001)
    timestep = timestep + 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
plt.show()
cv2.destroyAllWindows()
