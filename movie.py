import os
import cv2
import numpy as np
from collections import OrderedDict

from imutils import face_utils
import imutils
import dlib
from pathos.constants import *
from pathos.model import EmotionRecognitionModel



def draw_bounding_box(face_coordinates, image_array, color):
    x, y, w, h = face_coordinates
    cv2.rectangle(image_array, (x, y), (x + w, y + h), color, 2)

def apply_offsets(face_coordinates, offsets):
    x, y, width, height = face_coordinates
    x_off, y_off = offsets
    return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

def draw_text(coordinates, image_array, text, color, x_offset=0, y_offset=0,
                                                font_scale=2, thickness=2):
    x, y = coordinates[:2]
    cv2.putText(image_array, text, (x + x_offset, y + y_offset),
                cv2.FONT_HERSHEY_SIMPLEX,
                font_scale, color, thickness, cv2.LINE_AA)



def format_image(bgr_image):
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)

    gray_image = cv2.equalizeHist(gray_image)
    try:
        gray_image = cv2.resize(gray_image, (48, 48), interpolation=cv2.INTER_CUBIC) / 255.
    except Exception:
        print("[Error] While resizing")
        return None

    return gray_image




# Données sur la vidéo
cap = cv2.VideoCapture('gardian.mkv')
output_video = "test.avi"

skipped_frames = 11100
# parameters for loading data and images
detection_model_path = 'pathos/haarcascade_files/haarcascade_frontalface_default.xml'

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# define a dictionary that maps the indexes of the facial
# landmarks to specific face regions
FACIAL_LANDMARKS_IDXS = OrderedDict([
    ("mouth", (48, 68)),
    ("right_eyebrow", (17, 22)),
    ("left_eyebrow", (22, 27)),
    ("right_eye", (36, 42)),
    ("left_eye", (42, 48)),
    ("nose", (27, 35)),
    ("jaw", (0, 17))
])

# hyper-parameters for bounding boxes shape
frame_window = 10
emotion_offsets = (20, 40)

# loading models
face_detection = cv2.CascadeClassifier(detection_model_path)
# emotion_classifier = load_model(emotion_model_path, compile=False)

#emotion_network = EmotionRecognitionModel(os.path.join(SAVE_DIRECTORY, "network-1200"))
emotion_network = EmotionRecognitionModel("learn80")

emotion_network.build_network()

EMOTIONS = ['ANG', 'DIS', 'FEA', 'HAP', 'SAD', 'SUR', 'NTR']

cv2.namedWindow('Pathos v1.2a')

#video_capture = cv2.VideoCapture(0)

#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter(output_video,fourcc, 20.0, (640,480))
out = cv2.VideoWriter('output.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (int(cap.get(3)),int(cap.get(4))))

while(cap.isOpened()):
    # Capturing the image from webcam
    # bgr_image = video_capture.read()[1]

    _, bgr_image = cap.read()
    

    skipped_frames = skipped_frames - 1
    print("frames: " + str(skipped_frames))
    if(skipped_frames > 0):
        continue
    if(skipped_frames < - 800):
        break
    # Converting image to grayscale and rgb
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    faces = face_detection.detectMultiScale(gray_image, 1.3, 5)
    # Loop on each face
    for face_coordinates in faces:
        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        # gray_face = gray_image[y1:y2, x1:x2]
        focused_face_bgr = bgr_image[y1:y2, x1:x2]

        # gray_face = preprocess_input(gray_face, True)
        # gray_face2 = preprocess_input(gray_face2, True)
        # gray_face = np.expand_dims(gray_face, 0)
        # gray_face = np.expand_dims(gray_face, -1)

        # emotion_prediction = emotion_classifier.predict(gray_face)

        result = emotion_network.predict(format_image(focused_face_bgr))

        # for (proba, emotion_label, index) in zip(emotion_prediction[0], emotion_labels, range(len(emotion_labels))):
        # cv2.putText(rgb_image, emotion_label, (index * 86 + 10, 15), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 200, 200), 1)

        #    cv2.rectangle(rgb_image, (index * 86 + 10, 30), ((index + 1) * 86 + 4, 30 + int(proba * 100)) , (200, 200, 200), -1)
        if result is not None:
            emotion_probability = np.max(result)
            emotion_label_arg = np.argmax(result)
            emotion_text = EMOTIONS[emotion_label_arg]

            if emotion_text == 'angry':
                color = emotion_probability * np.asarray((255, 0, 0))
            elif emotion_text == 'sad':
                color = emotion_probability * np.asarray((0, 0, 255))
            elif emotion_text == 'happy':
                color = emotion_probability * np.asarray((255, 255, 0))
            elif emotion_text == 'surprise':
                color = emotion_probability * np.asarray((0, 255, 255))
            else:
                color = emotion_probability * np.asarray((0, 255, 0))

        if result is not None:
            for (proba, emotion, index) in zip(result[0], EMOTIONS, range(len(EMOTIONS))):
                cv2.rectangle(rgb_image, (face_coordinates[0] + face_coordinates[2], face_coordinates[1] + index * 30),
                              (face_coordinates[0] + face_coordinates[2] + int(proba * 100),
                               face_coordinates[1] + (index + 1) * 30), (200, 200, 200, 200), -1)
                draw_text(face_coordinates, rgb_image, emotion + "->" + str(int(proba * 100)), color,
                          face_coordinates[2] + 10, index * 30 + 18, 0.5, 1)

        # emotion_window.append(emotion_text)

        # if len(emotion_window) > frame_window:
        #    emotion_window.pop(0)
        # try:
        #    emotion_mode = mode(emotion_window)
        # except:
        #    continue

        # print(emotion_text)
            color = color.astype(int)
            color = color.tolist()

            draw_bounding_box(face_coordinates, rgb_image, color)
        # draw_text(face_coordinates, rgb_image, emotion_mode,
        #          color, 0, -45, 1, 1)

    bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
    # ============================================
    # LANDMARKS
    # ============================================
    #image_landmarks = imutils.resize(bgr_image, width=1200)
    image_landmarks = bgr_image
    #bgr_image = imutils.resize(bgr_image, width=1200)
    gray = cv2.cvtColor(image_landmarks, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    rects = detector(gray, 1)
    clone = None
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the landmark (x, y)-coordinates to a NumPy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        clone = image_landmarks.copy()
        # loop over the face parts individually
        draw_bounding_box((rect.left(), rect.top(), rect.width(), rect.height()), clone, (0, 0, 0))
        for (name, (i, j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
            # clone the original image so we can draw on it, then
            # display the name of the face part on the image
            # cv2.putText(clone, name, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
            #    0.7, (0, 0, 255), 2)

            # loop over the subset of facial landmarks, drawing the
            # specific face part
            for (x, y) in shape[i:j]:
                cv2.circle(clone, (x, y), 1, (0, 0, 255), -1)

            if name == "jaw":
                for l in range(i + 2, j):
                    ptA = tuple(shape[l - 1])
                    ptB = tuple(shape[l])
                    cv2.line(clone, ptA, ptB, (255, 0, 0), 1)

            # extract the ROI of the face region as a separate image
            # (x, y, w, h) = cv2.boundingRect(np.array([shape[i:j]]))
            # roi = image_landmarks[y:y + h, x:x + w]
            # roi = imutils.resize(roi, width=250, inter=cv2.INTER_CUBIC)

        # show the particular face part
        # cv2.imshow("ROI", roi)
        # cv2.imshow("Image", clone)
        # cv2.waitKey(0)

        # visualize all facial landmarks with a transparent overlay
        # output = face_utils.visualize_facial_landmarks(image_landmarks, shape)
        # cv2.imshow("Image", output)
    frame = None
    if clone is None:
        cv2.imshow('window_frame', bgr_image)
        frame = cv2.flip(bgr_image, 0)
    else:
        cv2.imshow('window_frame', clone)
        frame = cv2.flip(clone, 0)

    # write the flipped frame
    print("Writing frame")
    out.write(bgr_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
out.release()

# Closes all the frames
cv2.destroyAllWindows()
